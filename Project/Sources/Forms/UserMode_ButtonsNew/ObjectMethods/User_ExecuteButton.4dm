C_TEXT:C284($message;$text)
C_LONGINT:C283($selectedItem;$form)

Case of 
	: (Form event code:C388=On Clicked:K2:4)
		If (User_Execute_ab{User_currentTable_i})
			EDIT FORMULA:C806(Table:C252(User_currentTable_i)->;User_Formula_t)
			If (OK=1)
				C_POINTER:C301($ptr)
				$ptr:=Table:C252(User_currentTable_i)
				
				If (Records in set:C195("UserSet")#Records in table:C83($ptr->))
					USE SET:C118("UserSet")
				End if 
				//EXECUTE FORMULA(UserModeFormula)
				
				CREATE EMPTY SET:C140($ptr->;"LockedSet")
				APPLY TO SELECTION:C70($ptr->;EXECUTE FORMULA:C63(User_Formula_t))
				If (Records in set:C195("LockedSet")#0)
					$message:=Get localized string:C991("UserMode Locked")
					ALERT:C41($message)
					USE SET:C118("LockedSet")
				End if 
			End if 
			
		End if 
		
	: (Form event code:C388=On Alternative Click:K2:36)
		If (User_ExecuteAll_b)
			$text:=Get localized string:C991("UserMode ExecutePopup")  //  "Apply Formula; Execute Code"
			$selectedItem:=Pop up menu:C542($text)
		Else 
			$selectedItem:=1
		End if 
		
		Case of 
			: ($selectedItem=1)  // same as on clicked
				If (User_Execute_ab{User_currentTable_i})
					EDIT FORMULA:C806(Table:C252(User_currentTable_i)->;User_Formula_t)
					If (OK=1)
						C_POINTER:C301($ptr)
						$ptr:=Table:C252(User_currentTable_i)
						
						//If (Records in set("UserSet")#Records in table($ptr->))
						//USE SET("UserSet")
						//End if 
						//EXECUTE FORMULA(UserModeFormula)
						
						CREATE EMPTY SET:C140($ptr->;"LockedSet")
						APPLY TO SELECTION:C70($ptr->;EXECUTE FORMULA:C63(User_Formula_t))
						If (Records in set:C195("LockedSet")#0)
							$message:=Get localized string:C991("UserMode Locked")
							ALERT:C41($message)
							USE SET:C118("LockedSet")
						End if 
					End if 
					
				End if 
				
			: ($selectedItem=2)  // code
				$form:=Open form window:C675("User_Code")
				DIALOG:C40("User_Code")
				CLOSE WINDOW:C154($form)
				
		End case 
End case 