C_TEXT:C284($text)
C_LONGINT:C283($selectedItem)

Case of 
	: (Form event code:C388=On Clicked:K2:4)
		If (User_Report_ab{User_currentTable_i})
			QR REPORT:C197(Table:C252(User_currentTable_i)->;Char:C90(1);True:C214;True:C214)
		End if 
		
	: (Form event code:C388=On Alternative Click:K2:36)
		$text:=Get localized string:C991("UserMode ReportPopup")  //  "Report;Import;Export"
		$selectedItem:=Pop up menu:C542($text)
		
		Case of 
			: ($selectedItem=1)
				If (User_Report_ab{User_currentTable_i})
					QR REPORT:C197(Table:C252(User_currentTable_i)->;Char:C90(1);True:C214;True:C214)
				End if 
			: ($selectedItem=2)
				If (User_Import_ab{User_currentTable_i})
					ARRAY TEXT:C222($tableTitles;0)
					ARRAY LONGINT:C221($tableID;0)
					GET TABLE TITLES:C803($tableTitles;$tableID)
					ARRAY TEXT:C222($newtableTitles;1)
					ARRAY LONGINT:C221($newtableID;1)
					$newtableTitles{1}:=Table name:C256(User_currentTable_i)
					$newtableID{1}:=User_currentTable_i
					SET TABLE TITLES:C601($newtableTitles;$newtableID)
					IMPORT DATA:C665("";*)
					SET TABLE TITLES:C601($tableTitles;$tableID)
				End if 
			: ($selectedItem=3)
				If (User_Export_ab{User_currentTable_i})
					ARRAY TEXT:C222($tableTitles;0)
					ARRAY LONGINT:C221($tableID;0)
					GET TABLE TITLES:C803($tableTitles;$tableID)
					ARRAY TEXT:C222($newtableTitles;1)
					ARRAY LONGINT:C221($newtableID;1)
					$newtableTitles{1}:=Table name:C256(User_currentTable_i)
					$newtableID{1}:=User_currentTable_i
					SET TABLE TITLES:C601($newtableTitles;$newtableID)
					EXPORT DATA:C666("";*)
					SET TABLE TITLES:C601($tableTitles;$tableID)
				End if 
		End case 
End case 



