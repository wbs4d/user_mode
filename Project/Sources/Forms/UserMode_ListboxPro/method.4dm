C_TEXT:C284($name;$text1;$text2;$text3;$who;$t)
C_LONGINT:C283($pos;$mylong;$win_l;$selectedRecord_l)

Case of 
	: (Form event code:C388=On Load:K2:1)
		User_Init  // should have been done already...
		User_Listbox_Init
		
		If (User_currentTable_i>0)
			$name:=Table name:C256(User_currentTable_i)
			$pos:=Find in array:C230(User_TableNames_at;$name)
			If ($pos>0)
				User_TableNames_at:=$pos  // popup
			Else 
				User_TableNames_at:=1
			End if 
		End if 
		
	: (Form event code:C388=On Close Box:K2:21)
		CANCEL:C270
		
	: (Form event code:C388=On Double Clicked:K2:5)
		If (User_Modify_ab{User_currentTable_i})
			If (User_IsFormExisting(User_Forms_at{User_currentTable_i}))
				$selectedRecord_l:=Selected record number:C246(Table:C252(User_currentTable_i)->)
				CUT NAMED SELECTION:C334(Table:C252(User_currentTable_i)->;"User_memory")
				READ WRITE:C146(Table:C252(User_currentTable_i)->)
				USE SET:C118("UserSet")
				If (Locked:C147(Table:C252(User_currentTable_i)->))
					LOAD RECORD:C52(Table:C252(User_currentTable_i)->)
				End if 
				
				If (Locked:C147(Table:C252(User_currentTable_i)->))
					LOCKED BY:C353(Table:C252(User_currentTable_i)->;$mylong;$text1;$text2;$text3)
					$name:=Table name:C256(User_currentTable_i)
					$who:=$text1+"/"+$text2+" ("+$text3+")"
					$t:=Get localized string:C991("UserMode Locked")
					$t:=Replace string:C233($t;"$1";$name)
					$t:=Replace string:C233($t;"$2";$who)
					CONFIRM:C162($t)
					If (OK=1)
						LOAD RECORD:C52(Table:C252(User_currentTable_i)->)  // record lost by confirm redraw
						ONE RECORD SELECT:C189(Table:C252(User_currentTable_i)->)
						READ ONLY:C145(Table:C252(User_currentTable_i)->)
						DISPLAY SELECTION:C59(Table:C252(User_currentTable_i)->)
						READ WRITE:C146(Table:C252(User_currentTable_i)->)
					End if 
				Else 
					
					$win_l:=Open form window:C675("User_Input")
					DIALOG:C40("User_Input")
					CLOSE WINDOW:C154($win_l)
					If (OK=1)
						User_LoadRecord
					End if 
				End if 
				USE NAMED SELECTION:C332("User_memory")
				LISTBOX SELECT ROW:C912(*;"Listbox";$selectedRecord_l)
			End if 
		End if 
End case 