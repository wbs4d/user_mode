//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: UserMode_Show

// Displays the user mode
// legacy call (old syntax)

// Access: Shared

// Parameters: 
//   $1 : Long : table number to show (optional)
//   $2 : Boolean : true to allow free text execution (optional)

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thoma Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	C_LONGINT:C283(UserMode_Show; $1)
	C_BOOLEAN:C305(UserMode_Show; $2)
	UserMode_Show
End if 

Case of 
	: (Count parameters:C259=0)
		User_Show
		
	: (Count parameters:C259=1)
		User_Show($1)
		$pos:=New process:C317("User_Show2"; 0; "User Mode"; $1)
		
	: (Count parameters:C259=2)
		User_Show($1; $2)
		
End case 


