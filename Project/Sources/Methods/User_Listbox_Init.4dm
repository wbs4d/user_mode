//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: User_Listbox_Init

// Initialies the listbox after a new table is selected

// Access: Private

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	User_Listbox_Init
End if 

C_POINTER:C301($tableptr;$fieldptr;$headerptr;$ptr;$ptr2)
C_LONGINT:C283($countfields;$counter;$fieldID;$typ)
C_TEXT:C284($name)


If (Is table number valid:C999(User_currentTable_i))
	$tableptr:=Table:C252(User_currentTable_i)
	CREATE EMPTY SET:C140($tableptr->;"UserSet")
	
	User_Formula_t:=""
	
	If (User_Modify_ab{User_currentTable_i})
		READ WRITE:C146($tableptr->)
	Else 
		READ ONLY:C145($tableptr->)
	End if 
	ALL RECORDS:C47($tableptr->)
	
	LISTBOX DELETE COLUMN:C830(*;"Listbox";1;LISTBOX Get number of columns:C831(*;"Listbox"))
	LISTBOX SET TABLE SOURCE:C1013(*;"Listbox";User_currentTable_i)
	
	$countfields:=Get last field number:C255(User_currentTable_i)
	$counter:=1
	$fieldid:=1
	C_POINTER:C301($NilPtr)
	While (($fieldid<=$countfields) & ($counter<50))
		If (Is field number valid:C1000(User_currentTable_i;$fieldid))
			$Fieldptr:=Field:C253(User_currentTable_i;$fieldid)
			$typ:=Type:C295($Fieldptr->)
			If (($typ#Is picture:K8:10) & ($typ#Is BLOB:K8:12))
				//$Headerptr:=Get pointer("Header"+String($counter))
				//$Headerptr->:=0
				LISTBOX INSERT COLUMN:C829(*;"ListBox";$counter;"Field"+String:C10($counter);$Fieldptr->;"Header"+String:C10($counter);$NilPtr)
				OBJECT SET TITLE:C194(*;"Header"+String:C10($counter);Field name:C257($Fieldptr))
				OBJECT SET ENTERABLE:C238(*;"Field"+String:C10($counter);False:C215)
				$counter:=$counter+1
			End if 
		End if 
		$fieldid:=$fieldid+1
	End while 
	
	// Button handling
	If (User_Delete_ab{User_currentTable_i})
		OBJECT SET ENABLED:C1123(*;"User_DeleteButton";True:C214)
	Else 
		OBJECT SET ENABLED:C1123(*;"User_DeleteButton";False:C215)
	End if 
	If ((User_Report_ab{User_currentTable_i}) | (User_Import_ab{User_currentTable_i}) | (User_Export_ab{User_currentTable_i}))
		OBJECT SET ENABLED:C1123(*;"User_ReportButton";True:C214)
	Else 
		OBJECT SET ENABLED:C1123(*;"User_ReportButton";False:C215)
	End if 
	If (User_Execute_ab{User_currentTable_i})
		OBJECT SET ENABLED:C1123(*;"User_ExecuteButton";True:C214)
	Else 
		OBJECT SET ENABLED:C1123(*;"User_ExecuteButton";False:C215)
	End if 
	
	If (False:C215)  // *** Add K. 01/09/2017
		If (Not:C34(User_New_ab{User_currentTable_i}))
			OBJECT SET ENABLED:C1123(*;"User_NewButton";False:C215)
		Else 
			$name:=User_Forms_at{User_currentTable_i}
			If ($name="")
				$name:="UserMode"
				User_Forms_at{User_currentTable_i}:=$name
			End if 
			If (User_IsFormExisting($name))
				OBJECT SET ENABLED:C1123(*;"User_NewButton";True:C214)
			Else 
				User_Modify_ab{User_currentTable_i}:=False:C215
				OBJECT SET ENABLED:C1123(*;"User_NewButton";False:C215)
			End if 
		End if 
	End if 
	
	$ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Fieldname")
	$ptr2:=OBJECT Get pointer:C1124(Object named:K67:5;"Fieldcontent")
	If ($ptr#Null:C1517)
		CLEAR VARIABLE:C89($ptr->)
	End if 
	If ($ptr2#Null:C1517)
		CLEAR VARIABLE:C89($ptr2->)
	End if 
	
End if 