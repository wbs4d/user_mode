//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: User_Access (Tag; Pointer to Boolean Array)

// Allows you to specify access rights
// Pass a valid tag (Show, Report, Delete, Import, Export, New, Modify, Execute)
// and a boolean array, one row per table - True = allow
// The Show tag will allow this table to be shown in the User Mode component
// The other tags turn on/off functionality for that table once displayed

// Access: Shared

// Parameters: 
//   $1 : Text : Access Tag
//   $2 : Pointer : Pointer to a boolean array

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	C_POINTER:C301(User_Access;$2)
	C_TEXT:C284(User_Access;$1)
	
End if 

User_Init

If (Count parameters:C259=2)
	If ((Type:C295($1)=Is text:K8:3) | (Type:C295($1)=Is string var:K8:2) | (Type:C295($1)=Is alpha field:K8:1))
		If (Type:C295($2)=Is pointer:K8:14)
			If (Type:C295($2->)=Boolean array:K8:21)
				//%W-518.1
				Case of 
					: ($1="Show")
						COPY ARRAY:C226($2->;User_Show_ab)
						ARRAY BOOLEAN:C223(User_Show_ab;Get last table number:C254)
						User_InitTableNames
					: ($1="report")
						COPY ARRAY:C226($2->;User_Report_ab)
						ARRAY BOOLEAN:C223(User_Report_ab;Get last table number:C254)
					: ($1="Delete")
						COPY ARRAY:C226($2->;User_Delete_ab)
						ARRAY BOOLEAN:C223(User_Delete_ab;Get last table number:C254)
					: ($1="Import")
						COPY ARRAY:C226($2->;User_Import_ab)
						ARRAY BOOLEAN:C223(User_Import_ab;Get last table number:C254)
					: ($1="Export")
						COPY ARRAY:C226($2->;User_Export_ab)
						ARRAY BOOLEAN:C223(User_Export_ab;Get last table number:C254)
					: ($1="New")
						COPY ARRAY:C226($2->;User_New_ab)
						ARRAY BOOLEAN:C223(User_New_ab;Get last table number:C254)
					: ($1="Modify")
						COPY ARRAY:C226($2->;User_Modify_ab)
						ARRAY BOOLEAN:C223(User_Modify_ab;Get last table number:C254)
					: ($1="Execute")
						COPY ARRAY:C226($2->;User_Execute_ab)
						ARRAY BOOLEAN:C223(User_Execute_ab;Get last table number:C254)
				End case 
				//%W+518.1
			End if 
		End if 
	End if 
End if 