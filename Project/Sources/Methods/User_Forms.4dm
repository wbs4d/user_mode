//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: User_Forms (Pointer to text array)

// Allows the developer to specify the Form to be used for New/Double click

// Access: Shared

// Parameters: 
//   $1 : Pointer : Pointer to a text array (Form names)

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	C_POINTER:C301(User_Forms;$1)
End if 

User_Init

If (Count parameters:C259=1)
	If (Type:C295($1)=Is pointer:K8:14)
		If (Type:C295($1->)=Text array:K8:16)
			//%W-518.1
			COPY ARRAY:C226($1->;User_Forms_at)
			//%W+518.1
			ARRAY TEXT:C222(User_Forms_at;Get last table number:C254)
		End if 
	End if 
End if 