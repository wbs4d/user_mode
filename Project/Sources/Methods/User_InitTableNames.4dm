//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: User_InitTableNames

// InitTableNames array, needed after changing displayed/hidden tables

// Access: Private

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	User_InitTableNames
End if 

User_Init

C_LONGINT:C283($i)

User_currentTable_i:=0

ARRAY TEXT:C222(User_TableNames_at;0)
ARRAY POINTER:C280(User_TablePtrs_aptr;0)

For ($i;1;Size of array:C274(User_Show_ab))
	If (User_Show_ab{$i})
		If (Is table number valid:C999($i))
			If (User_currentTable_i=0)
				User_currentTable_i:=$i
			End if 
			APPEND TO ARRAY:C911(User_TableNames_at;Table name:C256($i))
			APPEND TO ARRAY:C911(User_TablePtrs_aptr;Table:C252($i))
		End if 
	End if 
End for 

SORT ARRAY:C229(User_TableNames_at;User_TablePtrs_aptr;>)