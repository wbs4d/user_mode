//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: User_Init

// Initialises the User Mode component, only runs once per process

// Access: Private

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------
If (False:C215)
	User_Init
End if 



C_LONGINT:C283($maxtables;$i)

C_OBJECT:C1216(UM)
If (UM=Null:C1517)
	
	Compiler_User
	
	UM:=New object:C1471("Initialised";True:C214)
	
	$maxtables:=Get last table number:C254
	ARRAY BOOLEAN:C223(User_Show_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Report_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Delete_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Import_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Export_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_New_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Modify_ab;$maxtables)
	ARRAY BOOLEAN:C223(User_Execute_ab;$maxtables)
	ARRAY TEXT:C222(User_Forms_at;$maxtables)
	
	For ($i;1;$maxtables)
		User_Show_ab{$i}:=True:C214
	End for 
	
	COPY ARRAY:C226(User_Show_ab;User_Report_ab)
	COPY ARRAY:C226(User_Show_ab;User_Delete_ab)
	COPY ARRAY:C226(User_Show_ab;User_Import_ab)
	COPY ARRAY:C226(User_Show_ab;User_Export_ab)
	COPY ARRAY:C226(User_Show_ab;User_New_ab)
	COPY ARRAY:C226(User_Show_ab;User_Modify_ab)
	COPY ARRAY:C226(User_Show_ab;User_Execute_ab)
	
	User_InitTableNames
	
End if 


