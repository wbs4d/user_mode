//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: User_LoadRecord

// After a record is selected in the list, this method
// loads the fields into the display to the right of the
// listing.

// Access: Private

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	User_LoadRecord
End if 



READ ONLY:C145(Table:C252(User_currentTable_i)->)
LOAD RECORD:C52(Table:C252(User_currentTable_i)->)
READ WRITE:C146(Table:C252(User_currentTable_i)->)
ARRAY TEXT:C222($fieldnames;0)
ARRAY OBJECT:C1221($fieldcontent;0)
For ($i;1;Get last field number:C255(Table:C252(User_currentTable_i)))
	If (Is field number valid:C1000(Table:C252(User_currentTable_i);$i))
		
		// handle 4D View Pro Object Array
		CLEAR VARIABLE:C89($itemObject)
		C_OBJECT:C1216($itemObject)
		$Fieldtyp:=Type:C295(Field:C253(User_currentTable_i;$i)->)
		$dontpostvalue:=False:C215
		$changelineheight:=False:C215
		Case of 
			: ($fieldtyp=Is real:K8:4)
				OB SET:C1220($itemObject;"valueType";"real")
				
			: (($fieldtyp=Is integer:K8:5) | ($fieldtyp=Is longint:K8:6))
				OB SET:C1220($itemObject;"valueType";"integer")
				
			: (($fieldtyp=Is date:K8:7))
				OB SET:C1220($itemObject;"valueType";"date")
				
			: (($fieldtyp=Is boolean:K8:9))
				OB SET:C1220($itemObject;"valueType";"boolean")
				
			: ($fieldtyp=Is alpha field:K8:1)
				OB SET:C1220($itemObject;"valueType";"text")
				
			: ($fieldtyp=Is text:K8:3)
				OB SET:C1220($itemObject;"valueType";"text")
				OB SET:C1220($itemObject;"alternateButton";True:C214)
				$changelineheight:=True:C214
				
				
			: ($fieldtyp=Is time:K8:8)
				OB SET:C1220($itemObject;"valueType";"time")
				
			: ($fieldtyp=Is picture:K8:10)
				OB SET:C1220($itemObject;"valueType";"event")
				OB SET:C1220($itemObject;"label";Get localized string:C991("UserMode EditPicture"))
				OB SET:C1220($itemObject;"_UC_table";User_currentTable_i)
				OB SET:C1220($itemObject;"_UC_field";$i)
				OB SET:C1220($itemObject;"_UC_type";"picture")
				$dontpostvalue:=True:C214
				
			: ($fieldtyp=Is object:K8:27)
				OB SET:C1220($itemObject;"valueType";"event")
				OB SET:C1220($itemObject;"label";Get localized string:C991("UserMode EditObject"))
				OB SET:C1220($itemObject;"_UC_table";User_currentTable_i)
				OB SET:C1220($itemObject;"_UC_field";$i)
				OB SET:C1220($itemObject;"_UC_type";"object")
				$dontpostvalue:=True:C214
				
			: ($fieldtyp=Is BLOB:K8:12)
				OB SET:C1220($itemObject;"valueType";"event")
				OB SET:C1220($itemObject;"label";Get localized string:C991("UserMode EditBlob"))
				OB SET:C1220($itemObject;"_UC_table";User_currentTable_i)
				OB SET:C1220($itemObject;"_UC_field";$i)
				OB SET:C1220($itemObject;"_UC_type";"blob")
				$dontpostvalue:=True:C214
		End case 
		
		If (Not:C34(OB Is empty:C1297($itemObject)))
			If (Not:C34($dontpostvalue))
				OB SET:C1220($itemObject;"value";Field:C253(User_currentTable_i;$i)->)
			End if 
			APPEND TO ARRAY:C911($fieldnames;Field name:C257(User_currentTable_i;$i))
			APPEND TO ARRAY:C911($fieldcontent;$itemObject)
			
			If ($changelineheight)
				// with v16 we could set now the size of the row to 3-5 lines...
				// LISTBOX SET ROW HEIGHT(*;"ListboxDetail";size of array($fieldnames);80)
			End if 
		End if 
	End if 
End for 

$ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Fieldname")
$ptr2:=OBJECT Get pointer:C1124(Object named:K67:5;"Fieldcontent")
//%W-518.1
COPY ARRAY:C226($fieldnames;$ptr->)
COPY ARRAY:C226($fieldcontent;$ptr2->)
//%W+518.1