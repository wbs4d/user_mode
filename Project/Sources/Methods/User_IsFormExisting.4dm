//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: User_IsFormExisting (Form Name) --> Boolean

// Verifies that a form with this name exists for the current table

// Access: Private

// Parameters: 
//   $1 : Text : The form Name

// Returns: 
//   $0 : Boolean : True = the form exists

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thomas Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------

If (False:C215)
	C_TEXT:C284(User_IsFormExisting;$1)
	C_BOOLEAN:C305(User_IsFormExisting;$0)
	
End if 

ARRAY TEXT:C222($arr_Names;0)
FORM GET NAMES:C1167(Table:C252(User_currentTable_i)->;$arr_Names;$1;*)  // update for v14
$0:=(Size of array:C274($arr_Names)>0)
