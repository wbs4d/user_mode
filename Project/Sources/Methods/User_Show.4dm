//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: User_Show {(Table Number {; Allow Code execution })}

// Displays the user mode

// Access: Shared

// Parameters: 
//   $1 : Long : table number to show (optional)
//   $2 : Boolean : true to allow free text execution (optional)

// Created by Wayne Stewart (2022-03-27T13:00:00Z)
// Based on prior work by Thoma Maul
//     wayne@4dsupport.guru
// ----------------------------------------------------



If (False:C215)
	C_LONGINT:C283(User_Show;$1)
	C_BOOLEAN:C305(User_Show;$2)
End if 


C_TEXT:C284($name;$useform;$windowtitle)
C_LONGINT:C283($pos;$newtable;$win)

User_Init

If (Macintosh option down:C545)
	Case of 
		: (Count parameters:C259=0)
			$pos:=New process:C317("User_Show2";0;"User Mode")
			
		: (Count parameters:C259=1)
			$pos:=New process:C317("User_Show2";0;"User Mode";$1)
			
		: (Count parameters:C259=2)
			$pos:=New process:C317("User_Show2";0;"User Mode";$1;$2)
			
	End case 
	
Else 
	
	User_ExecuteAll_b:=True:C214
	
	If (Count parameters:C259>0)
		If (($1>=1) & ($1<=Get last table number:C254))
			If (Is table number valid:C999($1))
				$name:=Table name:C256($1)
				$pos:=Find in array:C230(User_TableNames_at;$name)
				If ($pos>0)
					C_POINTER:C301($ptr)
					$ptr:=User_TablePtrs_aptr{$pos}
					$newtable:=Table:C252($ptr)
					If ($newtable#User_currentTable_i)
						User_currentTable_i:=$newtable
						User_Listbox_Init
					End if 
				End if 
			End if 
		End if 
	End if 
	
	If (Count parameters:C259>1)
		User_ExecuteAll_b:=$2
	End if 
	
	If (User_currentTable_i>0)
		$useform:="UserMode_ListboxPro"
		$windowtitle:=Get localized string:C991("UserMode WindowTitle")
		$win:=Open form window:C675($useform;Plain form window:K39:10;Horizontally centered:K39:1;*)
		DIALOG:C40($useform)
		CLOSE WINDOW:C154($win)
	End if 
	
End if 