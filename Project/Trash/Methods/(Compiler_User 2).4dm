//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_User
// Compiler variables related to the User routines.
// Method Type: Private
// Parameters: None
// Returns: Nothing
// Created by Wayne Stewart (2022-03-25T13:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


C_OBJECT:C1216(UM)

// Process Variables
If (UM=Null:C1517)  // So we only do this once.
	
	ARRAY BOOLEAN:C223(User_Show_ab;0)
	ARRAY BOOLEAN:C223(User_Report_ab;0)
	ARRAY BOOLEAN:C223(User_Delete_ab;0)
	ARRAY BOOLEAN:C223(User_Import_ab;0)
	ARRAY BOOLEAN:C223(User_Export_ab;0)
	ARRAY BOOLEAN:C223(User_New_ab;0)
	ARRAY BOOLEAN:C223(User_Modify_ab;0)
	ARRAY BOOLEAN:C223(User_Execute_ab;0)
	ARRAY TEXT:C222(User_Forms_at;0)
	ARRAY TEXT:C222(User_TableNames_at;0)
	ARRAY POINTER:C280(User_TablePtrs_aptr;0)
	
	C_BOOLEAN:C305(User_ExecuteAll_b)
	C_LONGINT:C283(User_CurrentTable_i)
	
	C_TEXT:C284(User_Code_t;User_Formula_t)
	
End if 

// Parameters
If (False:C215)
	
	C_TEXT:C284(User_IsFormExisting;$1)
	C_BOOLEAN:C305(User_IsFormExisting;$0)
	
	C_POINTER:C301(User_Forms;$1)
	
	C_TEXT:C284(User_ConvertFieldToText;$0)
	C_POINTER:C301(User_ConvertFieldToText;$1)
	C_BOOLEAN:C305(User_ConvertFieldToText;$2)
	
	C_POINTER:C301(User_Access;$2)
	C_TEXT:C284(User_Access;$1)
	
	C_LONGINT:C283(User_Show;$1)
	C_BOOLEAN:C305(User_Show;$2)
	
	C_LONGINT:C283(User_ShowSameProcess;$1)
	C_BOOLEAN:C305(User_ShowSameProcess;$2)
	
	C_TEXT:C284(_WriteDocumentation;$1)
	C_BOOLEAN:C305(_WriteDocumentation;$2;$3)
	
	
	
End if 
